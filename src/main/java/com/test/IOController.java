package com.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author Marcelino
 */
@Stateless
public class IOController implements Serializable {

    public boolean uploadFile(InputStream is, String path) throws FileNotFoundException {
        OutputStream out = new FileOutputStream(new File(path));
        try {
            int read = 0;
            byte[] bytes = new byte[8182];
            while ((read = is.read(bytes)) != -1) {
                out.write(bytes, 0, read);

            }
            out.close();
            return true;
        } catch (Exception e) {
            System.out.println("FILEuPLOADeRROR");
            return false;
        }

    }

    public String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

   
    
}
