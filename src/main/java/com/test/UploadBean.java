package com.test;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;



/**
 *
 * @author Marcelino
 */
@Named(value = "uploadBean")
@ViewScoped
public class UploadBean implements Serializable {
    @EJB
    IOController ioc;
    
    
    @PostConstruct
    public void init(){
        
    }
     public void handleFileUpload(FileUploadEvent evt) {
        String folderPath = "/Users/Marcelino/Desktop/pictures/rooms/" ;
        folderPath = folderPath.replaceAll(" ", "");
        File theFile = new File(folderPath);
        
        theFile.mkdirs();
        try {
            ioc.uploadFile(evt.getFile().getInputstream(), folderPath+evt.getFile().getFileName());
        } catch (IOException ex) {
            Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
